package com.fomaster.vgamepad.controller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.fomaster.vgamepad.R;
import com.fomaster.vgamepad.activities.Preferences;
import com.fomaster.vgamepad.bluetooth.BluetoothConnection;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by felip_000 on 17-01-2017.
 */

public class GamePad {

    private final int DPAD_LEFT_PRESSED      = 0x011;
    private final int DPAD_LEFT_RELEASED     = 0x001;
    private final int DPAD_RIGHT_PRESSED     = 0x012;
    private final int DPAD_RIGHT_RELEASED    = 0x002;
    private final int DPAD_UP_PRESSED        = 0x013;
    private final int DPAD_UP_RELEASED       = 0x003;
    private final int DPAD_DOWN_PRESSED      = 0x014;
    private final int DPAD_DOWN_RELEASED     = 0x004;

    private final int BUTTONPAD_X_PRESSED    = 0x1A1;
    private final int BUTTONPAD_X_RELEASED   = 0x0A1;
    private final int BUTTONPAD_B_PRESSED    = 0x1A2;
    private final int BUTTONPAD_B_RELEASED   = 0X0A2;
    private final int BUTTONPAD_Y_PRESSED    = 0x1A3;
    private final int BUTTONPAD_Y_RELEASED   = 0x0A3;
    private final int BUTTONPAD_A_PRESSED    = 0x1A4;
    private final int BUTTONPAD_A_RELEASED   = 0x0A4;

    private final int BUTTON_L1_PRESSED      = 0x1B1;
    private final int BUTTON_L1_RELEASED     = 0x0B1;
    private final int BUTTON_L2_PRESSED      = 0x1B2;
    private final int BUTTON_L2_RELEASED     = 0x0B2;
    private final int BUTTON_R1_PRESSED      = 0x1B3;
    private final int BUTTON_R1_RELEASED     = 0x0B3;
    private final int BUTTON_R2_PRESSED      = 0x1B4;
    private final int BUTTON_R2_RELEASED     = 0x0B4;

    private final int BUTTON_START_PRESSED   = 0x1C1;
    private final int BUTTON_START_RELEASED  = 0x0C1;
    private final int BUTTON_SELECT_PRESSED  = 0x1C2;
    private final int BUTTON_SELECT_RELEASED = 0x0C2;

    private Activity context;
    private BluetoothConnection connection;
    private boolean running;
    private ImageButton serverButton;
    private DataOutputStream out;
    private AsyncTask<Void, Void, Boolean> connectTask;
    private AsyncTask<Integer, Void, Void> touchHandler;

    public GamePad(final Activity context) {
        this.context = context;
        serverButton = (ImageButton) context.findViewById(R.id.ibConnect);
        connectTask = new ConnectionTask();

        serverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (connectTask.getStatus() != AsyncTask.Status.RUNNING) {
                    connectTask = new ConnectionTask();
                    if (!running) {
                        connection = new BluetoothConnection(
                                PreferenceManager.getDefaultSharedPreferences(context)
                                        .getString(context.getString(R.string.pref_key_server), "00:00:00:00:00"));

                        connectTask.execute();
                    } else {
                        connection.close();
                        out = null;
                        serverButton.setBackgroundColor(Color.RED);
                        running = false;
                    }
                }
            }
        });
        context.findViewById(R.id.ibMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, Preferences.class));
            }
        });

        initComponents();
    }

    private void initComponents() {
        setListeners(context.findViewById(R.id.dpad_left), DPAD_LEFT_PRESSED, DPAD_LEFT_RELEASED);
        setListeners(context.findViewById(R.id.dpad_right), DPAD_RIGHT_PRESSED, DPAD_RIGHT_RELEASED);
        setListeners(context.findViewById(R.id.dpad_up), DPAD_UP_PRESSED, DPAD_UP_RELEASED);
        setListeners(context.findViewById(R.id.dpad_down), DPAD_DOWN_PRESSED, DPAD_DOWN_RELEASED);

        setListeners(context.findViewById(R.id.bpad_X), BUTTONPAD_X_PRESSED, BUTTONPAD_X_RELEASED);
        setListeners(context.findViewById(R.id.bpad_B), BUTTONPAD_B_PRESSED, BUTTONPAD_B_RELEASED);
        setListeners(context.findViewById(R.id.bpad_Y), BUTTONPAD_Y_PRESSED, BUTTONPAD_Y_RELEASED);
        setListeners(context.findViewById(R.id.bpad_A), BUTTONPAD_A_PRESSED, BUTTONPAD_A_RELEASED);

        setListeners(context.findViewById(R.id.btn_L1), BUTTON_L1_PRESSED, BUTTON_L1_RELEASED);
        setListeners(context.findViewById(R.id.btn_L2), BUTTON_L2_PRESSED, BUTTON_L2_RELEASED);
        setListeners(context.findViewById(R.id.btn_R1), BUTTON_R1_PRESSED, BUTTON_R1_RELEASED);
        setListeners(context.findViewById(R.id.btn_R2), BUTTON_R2_PRESSED, BUTTON_R2_RELEASED);

        setListeners(context.findViewById(R.id.btn_Start), BUTTON_START_PRESSED, BUTTON_START_RELEASED);
        setListeners(context.findViewById(R.id.btn_Select), BUTTON_SELECT_PRESSED, BUTTON_SELECT_RELEASED);
    }

    private void setListeners(View view, final int buttonPress, final int buttonReleased) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return dispatcher(motionEvent, buttonPress, buttonReleased);
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return longDispatcher(buttonPress);
            }
        });
    }

    private boolean dispatcher(MotionEvent event, int actionPress, int actionReleased) {
        if (running) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    try {
                        out.writeInt(actionPress);
                    } catch (IOException e) {
                        connection.close();
                        out = null;
                        serverButton.setBackgroundColor(Color.RED);
                        running = false;
                    }

                    break;

                case MotionEvent.ACTION_UP:
                    if (touchHandler != null) touchHandler.cancel(true);
                    try {
                        out.writeInt(actionReleased);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        } else {
            return true;
        }

        return false;
    }

    private boolean longDispatcher(int actionPress) {
        if (running) {
            touchHandler = new LongTouchHandler();
            touchHandler.execute(actionPress);
        }

        return false;
    }

    private class ConnectionTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return connection.connect();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                out = connection.getOutputStream();
                serverButton.setBackgroundColor(Color.GREEN);
                running = true;
            } else {
                Toast.makeText(context, "Cannot Connect de Server", Toast.LENGTH_SHORT).show();
                running = false;
            }
            Log.i("Finish", "Finish task");
        }
    }

    private class LongTouchHandler extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... integers) {
            while (!isCancelled()) {
                try {
                    out.writeInt(integers[0]);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }
}
