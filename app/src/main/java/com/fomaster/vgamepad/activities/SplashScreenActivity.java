package com.fomaster.vgamepad.activities;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.fomaster.vgamepad.R;

/**
 * Created by felip_000 on 13-01-2017.
 */

public class SplashScreenActivity extends AppCompatActivity {

    private int REQUEST_ENABLE_BT = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(createBluetoothTask(), 1000);
    }

    private Runnable createBluetoothTask() {
        return new Runnable() {
            @Override
            public void run() {
                if (supportBluetooth()) {
                    requestEnable();
                } else {
                    Toast.makeText(SplashScreenActivity.this, "Your device not support bluetooth feature.", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private boolean supportBluetooth() {
        return BluetoothAdapter.getDefaultAdapter() != null;
    }

    private void requestEnable() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

        if (!adapter.isEnabled()) {
            Intent enable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enable, REQUEST_ENABLE_BT);
        } else {
            startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                finish();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Bluetooth not connected", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();

        if (hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }
}
