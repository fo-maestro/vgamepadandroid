package com.fomaster.vgamepad.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;
import android.widget.Toast;

import com.fomaster.vgamepad.R;
import com.fomaster.vgamepad.bluetooth.ServerAddress;

/**
 * Created by felip_000 on 17-01-2017.
 */

public class Preferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    PreferencesFragment fragment;
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, fragment = new PreferencesFragment()).commit();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    protected void onPause() {
        preferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(getString(R.string.etpref_key_assign_server))) {
            if (!sharedPreferences.getString(s, "").equals("")) {
                String summary = sharedPreferences.getString(s, "00:00:00:00:00");

                if (ServerAddress.isValid(summary)) {
                    sharedPreferences.edit().putString(getString(R.string.pref_key_server), summary).apply();
                    fragment.findPreference(getString(R.string.pref_key_server)).setSummary(summary);
                } else {
                    Toast.makeText(this, getString(R.string.toast_server_alert), Toast.LENGTH_SHORT).show();
                }

                ((EditTextPreference) fragment.findPreference(s)).setText("");
            }
        }
    }

    public static class PreferencesFragment extends PreferenceFragment  {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            findPreference(getString(R.string.pref_key_server)).setSummary(
                    getPreferenceManager().getSharedPreferences().getString(getString(R.string.pref_key_server), "00:00:00:00:00"));

            findPreference(getString(R.string.pref_key_about)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    SpannableString str = new SpannableString(getString(R.string.alert_about_message));
                    Linkify.addLinks(str, Linkify.WEB_URLS);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(getString(R.string.app_name))
                            .setMessage(str)
                            .setNegativeButton(getString(R.string.alert_about_negative_button), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    TextView view = (TextView) dialog.findViewById(android.R.id.message);
                    view.setMovementMethod(LinkMovementMethod.getInstance());

                    return true;
                }
            });
        }


    }
}
