package com.fomaster.vgamepad.bluetooth;

/**
 * Created by felip_000 on 20-01-2017.
 */

public final class ServerAddress {

    private ServerAddress() {}

    public static boolean isValid(String str) {
        if (str.length() < 17) return false;

        for (int i = 2; i < 15; i += 3) {
            if (str.charAt(i) != ':') return false;
        }

        String[] strSplit = str.split(":");

        for (String val : strSplit) {
            for (int i = 0; i < val.length(); i++) {
                if (Character.isLetter(val.charAt(i))) {
                    if (!Character.isUpperCase(val.charAt(i))) return false;
                } else if (!Character.isDigit(val.charAt(i))) {
                    return false;
                }
            }
        }

        return true;
    }
}
