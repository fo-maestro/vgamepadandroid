package com.fomaster.vgamepad.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by felip_000 on 23-01-2017.
 */

public class BluetoothConnection {

    private static final UUID BLUETOOTH_SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothAdapter adapter;
    private BluetoothDevice device;
    private BluetoothSocket connection;
    private DataOutputStream out;

    public BluetoothConnection(String serverAdress) {
        adapter = BluetoothAdapter.getDefaultAdapter();
        device = adapter.getRemoteDevice(serverAdress);
    }

    public boolean connect() {
        try {
            connection = device.createInsecureRfcommSocketToServiceRecord(BLUETOOTH_SPP_UUID);
            adapter.cancelDiscovery();

            connection.connect();
            out = new DataOutputStream(connection.getOutputStream());
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public DataOutputStream getOutputStream() { return out; }

    public void close() {
        try {
            if (out != null) out.close();
            if (connection != null) connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
