package com.fomaster.vgamepad.styles;

import android.content.Context;
import android.graphics.Color;
import android.util.Pair;

import com.fomaster.vgamepad.R;

import java.util.HashMap;

/**
 * Created by felip_000 on 17-01-2017.
 */

public class Style {

    private HashMap<String, Integer> styles;

    public Style(Context context, Theme theme) {
        styles = new HashMap<>();

        if (theme == Theme.DEFAUTL) {
            String[] vect = context.getResources().getStringArray(R.array.defaultTheme);

            for (String key : vect) {
                String[] spilter = key.split(",");
                styles.put(spilter[0], Color.parseColor(spilter[1]));
            }
        }
    }

    public Pair<String, Integer> getStyleElement(String styleName) {
        return (styles.containsKey(styleName)) ? new Pair<>(styleName, styles.get(styleName)) : null;
    }

    public enum Theme {
        DEFAUTL
    }
}
