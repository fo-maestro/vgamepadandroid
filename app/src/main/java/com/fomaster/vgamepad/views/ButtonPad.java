package com.fomaster.vgamepad.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.fomaster.vgamepad.R;

/**
 * Created by felip_000 on 14-01-2017.
 */

public class ButtonPad extends View {

    private Paint paint;
    public final int desiredWidth = 100;
    public final int desiredHeight = 100;
    private int width;
    private int height;
    private int color;
    private ButtonPadType type;

    public ButtonPad(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ButtonPad, 0, 0);
        color = a.getColor(R.styleable.ButtonPad_button_color, Color.parseColor("#000000"));
        type = ButtonPadType.from(a.getInt(R.styleable.ButtonPad_button_type, 0));
        a.recycle();

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);


        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawOval(new RectF(0, 0, width, height), paint);
        type.getPaint().setTextSize(height * 3 / 4);
        canvas.drawText(type.getChar(), width / 2, height * 3 / 4, type.getPaint());
        //drawShape(canvas);
    }

    private void drawShape(Canvas canvas) {
        type.getPaint().setTextSize(height * 3 / 4);
        switch (type) {
            case BUTTON_Y:
                canvas.drawText(type.getChar(), width / 2, height * 3 / 4, type.getPaint());
                break;

            case BUTTON_X:
                break;

            case BUTTON_B:
                canvas.drawOval(new RectF(width / 4, height / 4, width * 3 / 4, height * 3 / 4), type.getPaint());
                break;

            case BUTTON_A:
                canvas.drawLine(width / 4, height * 3 / 4, width * 3 / 4, height / 4, type.getPaint());
                canvas.drawLine(width / 4, height / 4, width * 3 / 4, height * 3 / 4, type.getPaint());
                break;
        }
    }

    public enum ButtonPadType {
        BUTTON_Y(0), BUTTON_X(1), BUTTON_B(2), BUTTON_A(3);

        private Paint paint;
        private String character;

        ButtonPadType(int value) {
            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setTextAlign(Paint.Align.CENTER);

            switch (value) {
                case 0:
                    character = "Y";
                    paint.setColor(Color.GREEN);
                    break;

                case 1:
                    character = "X";
                    paint.setColor(Color.parseColor("#FF69B4"));
                    break;

                case 2:
                    character = "B";
                    paint.setColor(Color.parseColor("#F06A6A"));
                    break;

                case 3:
                    character = "A";
                    paint.setColor(Color.parseColor("#0F89E0"));
                    break;
            }
        }

        public Paint getPaint() { return paint; }

        public String getChar() { return character; }

        public static ButtonPadType from(int value) {
            if (value == 0) return BUTTON_Y;
            if (value == 1) return BUTTON_X;
            if (value == 2) return BUTTON_B;
            if (value == 3) return BUTTON_A;

            return BUTTON_A;
        }
    }
}
